import numpy as np

import glob


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

import pickle



#load native texts
native_list = []

native_paths = glob.glob('./native/*')
for path in native_paths:
  f = open(path, 'r')
  native_list.append(' '.join(f.readlines()))


print(f"native texts: {len(native_list)}") 



#load interviewer texts
interviewer_list = []

interviewer_paths = glob.glob('./interviewer/*')
for path in interviewer_paths:
  f = open(path, 'r')
  interviewer_list.append(' '.join(f.readlines()))
print(f"interviewer texts: {len(interviewer_list)}")



X = [native_list + interviewer_list][0]
y = [0] * len(native_list) + [1] * len(interviewer_list)
print(f"len X: {len(X)}")
print(f"len y: {len(y)}")



# spliting data



train_size = 0.8

X_train, X_test, y_train, y_test = train_test_split(X, y,train_size=train_size)

print(f"y train len: {len(y_train)}")
print(f"y test len: {len(y_test)}")
print("---")
print(f"y train: {y_train}")
print(f"y test: {y_test}")



# vectorizing train
vectorizer = CountVectorizer()
docs = np.array(X_train)

# fitting BoW model
bag = vectorizer.fit_transform(docs)

# unique words
print(vectorizer.get_feature_names())

# pairs word : token
print(vectorizer.vocabulary_)

# BoW vectors
print(bag.toarray())
# BoW shape
print(bag.toarray().shape)


#fitting model
# vectorizing train data
train_bag = bag.toarray()
 

lr = LogisticRegression(C=100.0,  solver='lbfgs', multi_class='ovr')

lr.fit(train_bag, y_train)



#predicting
# vectorizing test dataset
test_bag = vectorizer.transform(X_test).toarray()

y_predicted = lr.predict(test_bag)
  
print(f"y test: {np.array(y_test)}")
print(f"y predicted: {y_predicted}")
print("---")
print(f"Accuracy: {metrics.accuracy_score(np.array(y_test), y_predicted)}")



#saving models
# saving vectorizer
with open("models/vectorizer2.pkl", "wb") as f:
    pickle.dump(vectorizer, f)

# saving Log Reg
with open("models/logreg2.pkl", "wb") as f:
    pickle.dump(lr, f)