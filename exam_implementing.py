import numpy as np

import glob


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

import pickle



#load texts
text_list = []
'''
!!!
The order order of texts is not lexicographical
!!!
'''

native_paths = glob.glob('./texts/*')
for path in native_paths:
  # print(path)
  f = open(path, 'r')
  text_list.append(' '.join(f.readlines()))


print(f"texts: {len(text_list)}")



# loading logreg
with open("models/logreg2.pkl", "rb") as f:
    lr = pickle.load(f)

# loading vectorizer
with open("models/vectorizer2.pkl", "rb") as f:
    vectorizer = pickle.load(f)


# vectorizing and predicting
predict_bag = vectorizer.transform(text_list).toarray()

y_predicted = lr.predict(predict_bag)

print(f"y predicted: {y_predicted}")


def native_or_interviewer(predicted: np.array):
    ans = []
    for p in predicted:
        if (p == 0):
            ans.append('n')
        else:
            ans.append('i')

    return(ans)


print(native_or_interviewer(y_predicted))
print("\n\n")

for path, pred in zip(native_paths, native_or_interviewer(y_predicted)):
    print(f"{path}: {pred}")
