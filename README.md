# 5th semester AI NLP exam

## Task: Classify native speakers and interviewers texts.

## Project structure:

1. native - folder with native speaker texts

2. interviewer - folder with native speaker texts

3. text - texts for classification(contains all texts from native and interviewer folders)

4. models - contains logistic regression and bag-of-words models

5. exam-training.ipynb - notebook to train models

6. exam-implementing.ipynb - notebook to classify texts

### Code description:

#### exam-training

1. reading texts

2. spliting texts into validation and train datasets

3. vectorizing train dataset using bag of words

4. fitting logistic regression model

5. vectorizing test dataset using BoW

6. predicting values on a test dataset to validate model

7. saving models

#### exam-implementing

1. reading texts

2. loading models from exam-training

3. vectorizing texts

4. predicting class



### Files output:

#### exam-training

1. scikit-learn Bag-of-Words model

2. scikit-learn logistic regression model

#### exam-implementing

prints prediction for each file in ./texts/\*



filepath: prediction (n or i)

**examples:**

./texts/native: n

./texts/interv: i
